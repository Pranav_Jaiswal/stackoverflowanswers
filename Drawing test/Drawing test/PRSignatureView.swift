//
//  PJSignatureView.swift
//  PJSignature
//
//  Created by Pranav Jaiswal on 8/15/15.
//  Copyright (c) 2015 Pranav Jaiswal. All rights reserved.
//

import Foundation
import UIKit
import QuartzCore

class PRSignatureView: UIView
{
    
    var drawingColor:CGColorRef = UIColor.blackColor().CGColor //Col
    var drawingThickness:CGFloat = 0.5
    var drawingAlpha:CGFloat = 1.0
    
    var isEraserSelected: Bool
    
    private var currentPoint:CGPoint?
    private var previousPoint1:CGPoint?
    private var previousPoint2:CGPoint?
    
    private var path:CGMutablePathRef = CGPathCreateMutable()

    var image:UIImage?
    
    required init?(coder aDecoder: NSCoder) {
        //self.backgroundColor = UIColor.clearColor()
        self.isEraserSelected = false
        super.init(coder: aDecoder)
        self.backgroundColor = UIColor.clearColor()
    }
    
    override func drawRect(rect: CGRect)
    {
        self.isEraserSelected ? self.eraseMode() : self.drawingMode()
    }
    
    private func drawingMode()
    {
        if (self.image != nil)
        {
            self.image!.drawInRect(self.bounds)
        }
        let context:CGContextRef = UIGraphicsGetCurrentContext()!
        CGContextAddPath(context, path)
        CGContextSetLineCap(context, CGLineCap.Round)
        CGContextSetLineWidth(context, self.drawingThickness)
        CGContextSetStrokeColorWithColor(context, drawingColor)
        CGContextSetBlendMode(context, CGBlendMode.Normal)
        CGContextSetAlpha(context, self.drawingAlpha)
        CGContextStrokePath(context);
    }
    
    private func eraseMode()
    {
        if (self.image != nil)
        {
            self.image!.drawInRect(self.bounds)
        }
        let context:CGContextRef = UIGraphicsGetCurrentContext()!
        CGContextSaveGState(context)
        CGContextSetLineWidth(context, self.drawingThickness)
        CGContextAddPath(context, path);
        CGContextSetStrokeColorWithColor(context, UIColor.clearColor().CGColor)
        CGContextSetBlendMode(context, CGBlendMode.Clear)
        CGContextSetAlpha(context, self.drawingAlpha)
        CGContextSetLineCap(context, CGLineCap.Round)
        CGContextStrokePath(context)
        CGContextRestoreGState(context)
        
        /*context.setLineWidth(line.width)
        context.addPath(line.path)
        context.setStrokeColor(UIColor.clear.cgColor)
        context.setBlendMode(.clear)
        context.setAlpha(line.opacity)
        context.setLineCap(.round)
        context.strokePath()*/

    }

    
    
    
    private func midPoint (p1:CGPoint, p2:CGPoint)->CGPoint
    {
        return CGPointMake((p1.x + p2.x) * 0.5, (p1.y + p2.y) * 0.5)
    }
    
    private func finishDrawing()
    {
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, 0.0);
        drawViewHierarchyInRect(self.bounds, afterScreenUpdates: true)
        self.image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
    }
    
    func clearSignature()
    {
        path = CGPathCreateMutable()
        self.image = nil;
        self.setNeedsDisplay();
    }
    
    // MARK: - Touch Delegates
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        path = CGPathCreateMutable()
        let touch = touches.first!
        previousPoint1 = touch.previousLocationInView(self)
        currentPoint = touch.locationInView(self)
    }
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        let touch = touches.first!
        previousPoint2 = previousPoint1
        previousPoint1 = touch.previousLocationInView(self)
        currentPoint = touch.locationInView(self)
        
        let mid1 = midPoint(previousPoint2!, p2: previousPoint1!)
        let mid2 = midPoint(currentPoint!, p2: previousPoint1!)
        
        let subpath:CGMutablePathRef = CGPathCreateMutable()
        CGPathMoveToPoint(subpath, nil, mid1.x, mid1.y)
        CGPathAddQuadCurveToPoint(subpath, nil, previousPoint1!.x, previousPoint1!.y, mid2.x, mid2.y)
        CGPathAddPath(path, nil, subpath);
        self.setNeedsDisplay()
    }
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.touchesMoved(touches, withEvent: event)
        self.finishDrawing()
    }
    override func touchesCancelled(touches: Set<UITouch>?, withEvent event: UIEvent?) {
        self.touchesMoved(touches!, withEvent: event)
        self.finishDrawing()
    }
    
    
}
