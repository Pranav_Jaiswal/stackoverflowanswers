
//
//  ViewController.swift
//  Drawing test
//
//  Created by Pranav Jaiswal on 11/12/16.
//  Copyright © 2016 Pranav. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var drawButton: UIButton!
    @IBOutlet weak var signView: PRSignatureView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        configureSignViewSettings()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func configureSignViewSettings() {
        self.signView.drawingColor = UIColor.blackColor().CGColor
        self.signView.drawingAlpha = 1.0
        self.signView.drawingThickness = 10.0
    }

    @IBAction func drawTapped(sender: AnyObject) {
        self.signView.isEraserSelected = false
    }
    @IBAction func ersaserTapped(sender: AnyObject) {
        self.signView.isEraserSelected = true
    }
}

